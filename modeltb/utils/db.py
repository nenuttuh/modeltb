import os
import logging

from modeltb import settings
from contextlib import contextmanager
from functools import wraps


logger = logging.getLogger(__name__)


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = settings.Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def provide_session(func):
    """A decorator that adds a session to function kwargs.

    If a session is already provided in kwargs (and is not None),
    that will be used.
    """
    @wraps(func)
    def with_session(*args, **kwargs):
        if 'session' in kwargs:
            return func(*args, **kwargs)
        else:
            with session_scope() as session:
                kwargs['session'] = session
                return func(*args, **kwargs)

    return with_session


def _get_ext_version_path():
    try:
        ext_version_path = os.path.join(os.path.dirname(settings.MODEL_PKG.__file__),
                'migrations', 'versions')
    except:
        ext_version_path = None

    return ext_version_path


def get_alembic_config():
    """Create and return the config object."""
    # Lazy import alembic stuff to save time when importing the module
    from alembic.config import Config

    current_dir = os.path.dirname(os.path.abspath(__file__))
    package_dir = os.path.normpath(os.path.join(current_dir, '..'))
    alembic_dir = os.path.join(package_dir, 'migrations')
    config = Config(os.path.join(package_dir, 'alembic.ini'))
    config.set_main_option('script_location', alembic_dir)
    config.set_main_option('sqlalchemy.url', settings.SQLALCHEMY_URL)

    # Add external versions location if existing. Migrations for a module
    # need to reside in <module_root>/migrations/versions directory.
    if settings.MODEL_PKG:
        ext_version_loc = _get_ext_version_path()
        version_locations = "%s %s" % (ext_version_loc, '%(here)s/migrations/versions')
        config.set_main_option('version_locations', version_locations)

    return config


def makemigrations(msg='auto'):
    """Create a new revision for the external module.

    If no versions exist, create a labeled base revision.
    """
    from alembic import command
    from alembic.util.exc import CommandError
    from alembic.script import ScriptDirectory

    # If external module not defined, do nothing
    if not settings.MODEL_PKG:
        logger.warn("Modeltb makemigrations: settings.MODEL_PKG not defined, do nothing.")
        return None

    config = get_alembic_config()
    script = ScriptDirectory.from_config(config)
    # Use the external module name as branch_label
    branch_label = settings.MODEL_PKG.__name__

    # If the branch label does not exist, create a labeled base revision.
    try:
        # Raises CommandError if branch with label does not exist
        script.get_revision(branch_label)
    except CommandError:
        command.revision(config=config,
                message="create branch %s and %s" % (branch_label, msg),
                head='base',
                autogenerate=True,
                branch_label=branch_label,
                version_path=_get_ext_version_path())
        return None

    # If branch exists, generate new revision to <ext_module>@head
    command.revision(config=config,
            message=msg,
            head="%s@head" % branch_label,
            autogenerate=True)
    return None


def createdb():
    """Create database from scratch and set current to 'head'."""
    from alembic import command
    from modeltb import models

    logger.warn("Creating SQLAlchemy tables from scratch...")
    # Create all tables with SQLAlchemy's create_all()
    models.Base.metadata.create_all(settings.engine)
    config = get_alembic_config()
    # Tell Alembic that the newly created setup is the head
    command.stamp(config, 'heads')


def upgradedb():
    """Upgrade all branches to head."""
    from alembic import command
    
    logger.info("Running alembic migrations...")
    config = get_alembic_config()
    command.upgrade(config, 'heads')


def resetdb():
    """Drop all data and recreate the tables."""
    from alembic.migration import MigrationContext
    from modeltb import models

    logger.info("Dropping all data and recreating tables...")

    models.Base.metadata.drop_all(settings.engine)
    migrcontext = MigrationContext.configure(settings.engine)
    if migrcontext._version.exists(settings.engine):
        migrcontext._version.drop(settings.engine)

    upgradedb()
