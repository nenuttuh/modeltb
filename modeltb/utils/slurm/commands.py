import subprocess
import time
import uuid
import os
import logging
from modeltb.utils.slurm import templates
from modeltb.utils.db import provide_session
from modeltb import settings, exceptions
from modeltb.configuration import conf


logger = logging.getLogger(__name__)


SBATCH_COMMAND = 'sbatch'
STATE_POLL_INTERVAL = 15


def write_script_file(script_str, script_file):
    """Write the script into a file and return the file path.

    The file will be saved into settings.SLURM_SCRIPT_DIR/testbench_<tb_id>_<uuid.uuid4()>
    The uuid part is included so that the files will not be overwritten if run for the same
    testbench. SLURM_SCRIPT_DIR defaults to MODELTB_HOME/slurm_scripts

    Returns:
        The path of the created file.
    """
    filepath = os.path.join(settings.SLURM_SCRIPT_DIR, script_file)

    with open(filepath, 'w') as f:
        f.write(script_str)

    return filepath


def create_script_str(tb):
    """Format slurm template for given tb.

    Function fetches the slurm configuration from modeltb conf.

    Returns:
        The script contents and filename as a tuple (contents, filename).
    """
    # slurm_template is a mako template
    template = templates.slurm_template

    if tb.gpu:
        slurm_conf = conf['slurm_gpu']
    else:
        slurm_conf = conf['slurm_cpu']

    script_file_prefix = "testbench_%s_%s" % (str(tb.id), str(uuid.uuid4()))
    script_file = script_file_prefix + '.sh'
    output_file = os.path.join(settings.SLURM_SCRIPT_DIR, 'logs', script_file_prefix + '.out')
    error_file = os.path.join(settings.SLURM_SCRIPT_DIR, 'logs', script_file_prefix + '.error')

    try:
        model_pkg = settings.MODEL_PKG.__name__
    except:
        model_pkg = ''

    script_str = template.render(
        testbench_id=tb.id,
        job_name='run_tb_%s' % str(tb.id),
        output_file=output_file,
        error_file=error_file,
        model_pkg=model_pkg,
        modeltb_home=settings.MODELTB_HOME,
        **slurm_conf
    )

    return script_str, script_file


@provide_session
def run_tb_in_slurm(tb, session=None):
    """Run a testbench in Slurm.

    Params:
        testbench_id: The id of the testbench object to be run.

    Returns:
        True if SUCCESS, False if FAILED
    """
    script_str, script_file = create_script_str(tb)
    script_path = write_script_file(script_str, script_file)

    # Set tb.slurm to False so that tb.run() will not re-enter here.
    tb.slurm = False
    session.add(tb)
    session.commit()

    try:
        # Run the sbatch command
        output = subprocess.run(
            # [SBATCH_COMMAND, script_path],
            # When shell=True, the command needs to be provided as a string.
            # Note that this may have security issues because of possible
            # shell injections.
            "{} {}".format(SBATCH_COMMAND, script_path),
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
    except:
        tb.state = 'FAILED'
        session.add(tb)
        session.commit()
        raise

    # Log the output
    logger.info("Slurm output: \n%s" % output.stdout.decode('utf-8').strip())

    # If the command did not succeed, set to FAILED and raise ModeltbException
    if output.returncode:
        tb.state = 'FAILED'
        session.add(tb)
        session.commit()
        raise exceptions.ModeltbException("Testbench run in slurm failed")

    # Return true if testbench succeeded/failed
    def poke():
        # Add and commit to fetch changes
        session.add(tb)
        session.commit() 
        if tb.state in ('SUCCESS', 'FAILED'):
            return True
        return False

    # Poll until the testbench is done
    while not poke():
        time.sleep(STATE_POLL_INTERVAL)

    session.add(tb)
    session.commit()
    state = tb.state
    return True if state == 'SUCCESS' else False
