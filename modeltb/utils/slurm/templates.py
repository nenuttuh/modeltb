import os
from mako.template import Template


# The template directory should reside in the same folder with this script
templates_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'templates'
        )

slurm_template = Template(filename=os.path.join(templates_dir, 'slurm.template'))
