import atexit
import logging
import os

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.pool import NullPool
from modeltb import configuration as config
from modeltb.utils.multiprocessinglog import MultiProcessingLog


logger = logging.getLogger(__name__)


MODELTB_HOME = config.conf.get('modeltb', 'modeltb_home')
SQLALCHEMY_URL = config.conf.get('database', 'sqlalchemy_url')

# Log to MODELTB_HOME for now
LOG_FILE_PATH = os.path.join(MODELTB_HOME, 'logs', 'modeltb.log')
config.mkdir_p(os.path.dirname(LOG_FILE_PATH))
# Limit log file size to 512 kilobytes
LOG_MAX_BYTES = 1024 * 512

try:
    SLURM_SCRIPT_DIR = config.conf.get('slurm', 'scripts_dir')
except:
    SLURM_SCRIPT_DIR = os.path.join(MODELTB_HOME, 'slurm_scripts')
config.mkdir_p(os.path.join(SLURM_SCRIPT_DIR, 'logs'))

MODEL_PKG = None

engine = None
Session = None


def configure_orm():
    logger.debug("Setting up ORM...")
    global engine
    global Session
    engine_args = {}

    engine_args['poolclass'] = NullPool
    engine = create_engine(SQLALCHEMY_URL, **engine_args)

    Session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


def dispose_orm():
    """Close all connections on exit."""
    logger.debug("Disposing SQLAlchemy connections...")
    global engine
    global Session

    if Session:
        Session.remove()
        Session = None
    if engine:
        engine.dispose()
        engine = None


def configure_logging():
    """Set separate handlers for console and file.

    The file logger should handle multiprocessing issues.
    """
    # Configure the root logger, which others will inherit
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Create console handler
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)

    # Create multiprocessing handler that logs to single location
    mploghandler = MultiProcessingLog(LOG_FILE_PATH, 'a', LOG_MAX_BYTES, 5)
    mploghandler.setLevel(logging.INFO)

    # Set proper formatting
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    mploghandler.setFormatter(formatter)
    console.setFormatter(formatter)

    logger.addHandler(console)
    logger.addHandler(mploghandler)


def load_test_settings():
    """Read unittest configuration and reconfigure orm with the test config."""
    global SQLALCHEMY_URL
    config.load_test_config()
    SQLALCHEMY_URL = config.conf.get('database', 'sqlalchemy_url')
    configure_orm()


configure_logging()
configure_orm()
atexit.register(dispose_orm)
