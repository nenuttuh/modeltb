import logging
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from sqlalchemy import Boolean, Column, DateTime, Enum, Float, ForeignKey, \
        Integer, MetaData, String
from modeltb.exceptions import ModeltbException
from modeltb.utils.db import provide_session
from modeltb.utils.slurm import commands as slurm_commands


logger = logging.getLogger(__name__)


class Base(object):
    """A base class for declarative base."""
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=func.now())
    modified_at = Column(DateTime, default=func.now(), onupdate=func.now())


# This is done so that the constraints would have unique names. See
# http://alembic.zzzcomputing.com/en/latest/naming.html#autogen-naming-conventions
meta = MetaData(naming_convention={
        "ix": "ix_%(column_0_label)s",
        "uq": "uq_%(table_name)s_%(column_0_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s"
})
Base = declarative_base(cls=Base, metadata=meta)


class Testbench(Base):
    """Base class for building test benches."""
    # The seed and test set size for the dataset
    seed = Column(Integer)
    test_size = Column(Float, default=0.1)

    # Test performance as a single float. TODO: is this sensible at all? Can it be overridden?
    score = Column(Float)

    # The state information used by TBRunner
    state = Column(Enum('WAIT', 'RUN', 'SUCCESS', 'FAILED', name='state_enum'), default='WAIT')

    # A flag indicating whether the testbench will be run in Slurm
    slurm = Column(Boolean(name='slurm_bool'), default=False)

    # If True, run on GPU if available
    gpu = Column(Boolean(name='gpu_bool'), default=False)

    # One-to-one relations to Dataset and Model
    dataset = relationship('Dataset', uselist=False, back_populates='testbench')
    model = relationship('Model', uselist=False, back_populates='testbench')

    tbmetrics = relationship('TBMetric', back_populates='testbench')

    # The discriminator and configuration used in inheritance
    type_ = Column(String(50))
    __mapper_args__ = {
        'polymorphic_identity': 'testbench',
        'polymorphic_on': type_
    }

    def __init__(self, seed=None, test_size=None, model=None, dataset=None, **kwargs):
        if test_size:
            self.test_size = test_size
        self.seed = seed
        self.model = model
        self.dataset = dataset

    @provide_session
    def run(self, session=None, slurm=False):
        """Get the data, fit the model and assess model performance.

        dataset and model fields need to be defined and can be queried to
        return corresponding Dataset and Model instances.

        Args:
            session: The sqlalchemy Session object. Provided automatically if not defined.
            slurm: A boolean value indicating if the testbench should be run in Slurm.

        Returns:
            The object itself
        """
        # Run in Slurm, if configured
        if slurm or self.slurm:
            logger.info("Testbench.run: slurm configuration found for tb id %s, submitting to slurm"
                    % self.id)
            try:
                slurm_commands.run_tb_in_slurm(self)
            except ModeltbException as me:
                logger.warning(me)

            return self

        self.state = 'RUN'
        logger.info("Testbench.run: Running %s with id %s" % (self.__class__.__name__, str(self.id)))
        if not self.model:
            self.state = 'FAILED'
            session.add(self)
            session.commit()
            raise ModeltbException('Testbench.model needs to be defined before Testbench.run()')
        if not self.dataset:
            self.state = 'FAILED'
            session.add(self)
            session.commit()
            raise ModeltbException('Testbench.dataset needs to be defined before Testbench.run()')

        # Get the dataset
        train_data, test_data = self.dataset.train_test_split(self.test_size, self.seed)

        # Fit the model
        self.model.fit(train_data)

        # Assess performance
        session.add(self)  # Need to add to session, detached in self.model.fit()
        y_pred = self.model.predict(test_data)
        self.score = self.get_score(y_pred, test_data)

        # Save the results
        self.save_results(y_pred, test_data)

        self.state = 'SUCCESS'
        session.add(self)
        session.add(self.model)
        session.commit()
        return self

    @classmethod
    def get_score(cls, y_pred, y_true):
        raise NotImplementedError

    def save_results(self, y_pred, y_true):
        raise NotImplementedError


class Dataset(Base):
    """Dataset API used by Testbench."""
    type_ = Column(String(50))

    testbench_id = Column(Integer, ForeignKey('testbench.id'))
    testbench = relationship('Testbench', back_populates='dataset')

    __mapper_args__ = {
        'polymorphic_identity': 'dataset',
        'polymorphic_on': type_
    }

    def train_test_split(self, test_size=0.1, seed=None):
        """Get random train test split with optional seed.
        
        Args:
            test_size: A float indicating the portion of test set
                in the dataset.
            seed: The seed for random number generator

        Returns:
            A tuple (X_train, X_test, y_train, y_test) containing data
            and labels for train and test sets
        """
        raise NotImplementedError


class Model(Base):
    """Model API used by Testbench."""
    type_ = Column(String(50))

    testbench_id = Column(Integer, ForeignKey('testbench.id'))
    testbench = relationship('Testbench', back_populates='model')

    epochmetrics = relationship('EpochMetric', back_populates='model')

    __mapper_args__ = {
        'polymorphic_identity': 'model',
        'polymorphic_on': type_
    }
    
    def fit(self, xs, ys):
        """Fit the data into model."""
        raise NotImplementedError

    def predict(self, xs):
        """Predict the ys for xs."""
        raise NotImplementedError


class Metric(Base):
    type_ = Column(String(50))

    __mapper_args__ = {
        'polymorphic_identity': 'metric',
        'polymorphic_on': type_
    }

    name = Column(String(50))
    value = Column(Integer)

    def __str__(self):
        return "%s: %s" % (self.name, self.value)


class EpochMetric(Metric):
    id = Column(Integer, ForeignKey('metric.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'epochmetric'}

    model_id = Column(Integer, ForeignKey('model.id'))
    model = relationship('Model', back_populates='epochmetrics')
    
    # Would make this too nullable=False, but SQLite does not allow
    # adding not NULL cols into existing tables...
    epoch = Column(Integer)

    def __str__(self):
        return "Epoch %s %s: %s" % (self.epoch, self.name, self.value)


class TBMetric(Metric):
    id = Column(Integer, ForeignKey('metric.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'tbmetric'}

    testbench_id = Column(Integer, ForeignKey('testbench.id'))
    testbench = relationship('Testbench', back_populates='tbmetrics')
