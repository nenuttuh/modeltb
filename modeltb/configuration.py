"""Create modeltb configurations starting from default and test configs.

Test and default configuration files are created from templates, and MODELTB_HOME/modeltb.cfg
is then read (if existing, otherwise default conf is used and MODELTB_HOME will be ~/modeltb).
"""
import os
import errno
import logging

from configparser import ConfigParser
from modeltb.exceptions import ModeltbConfigException


logger = logging.getLogger(__name__)


templates_dir = os.path.join(os.path.dirname(__file__), 'config_templates')
with open(os.path.join(templates_dir, 'default_modeltb.cfg')) as f:
    DEFAULT_CONFIG = f.read()
with open(os.path.join(templates_dir, 'default_test.cfg')) as f:
    TEST_CONFIG = f.read()


class ModeltbConfigParser(ConfigParser):

    def __init__(self, *args, **kwargs):
        ConfigParser.__init__(self, *args, **kwargs)
        self.read_string(from_template(DEFAULT_CONFIG))

    def load_test_config(self):
        self.read_string(from_template(DEFAULT_CONFIG))
        self.read_string(from_template(TEST_CONFIG))
        self.read(TEST_CONFIG_FILE)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise ModeltbConfigException('Could not create directory')


def from_template(template):
    all_vars = {k: v for d in [globals(), locals()] for k, v in d.items()}
    return template.format(**all_vars)


if 'MODELTB_HOME' not in os.environ:
    MODELTB_HOME = os.path.expanduser('~/modeltb')
else:
    MODELTB_HOME = os.path.expanduser(os.path.expandvars(os.environ['MODELTB_HOME']))


mkdir_p(MODELTB_HOME)


MODELTB_CONFIG = os.path.join(MODELTB_HOME, 'modeltb.cfg')
TEST_CONFIG_FILE = os.path.join(MODELTB_HOME, 'unittests.cfg')


if not os.path.isfile(TEST_CONFIG_FILE):
    logger.info('Creating test config file in {}'.format(TEST_CONFIG_FILE))
    with open(TEST_CONFIG_FILE, 'w') as f:
        cfg = from_template(TEST_CONFIG)
        f.write(cfg)


if not os.path.isfile(MODELTB_CONFIG):
    logger.info('Creating config file in {}'.format(MODELTB_CONFIG))
    with open(MODELTB_CONFIG, 'w') as f:
        cfg = from_template(DEFAULT_CONFIG)
        f.write(cfg)


logger.info('Reading the configuration from {}'.format(MODELTB_CONFIG))


conf = ModeltbConfigParser()
conf.read(MODELTB_CONFIG)


def load_test_config():
    conf.load_test_config()
