from modeltb import settings
from modeltb.models import Testbench
from modeltb.exceptions import ModeltbException
from modeltb.utils.db import provide_session
import multiprocessing
import logging
import time


logger = logging.getLogger(__name__)


@provide_session
def _run_testbench(tb_id, session=None):
    """Fetch a single testbench and call tb.run()."""
    # Should find exactly one tb with the id
    tb = session.query(Testbench).filter(Testbench.id==tb_id).one()
    logger.info("_run_testbench() with id %s" % str(tb_id))
    try:
        tb.run()
        return tb_id
    except:
        session.add(tb)
        tb.state = 'FAILED'
        session.commit()
        raise


def _tb_success_callback(tb_id):
    logger.info("_run_testbench with id %s finished" % tb_id)


def _tb_error_callback(err):
    logger.error("Error in _run_testbench: %s" % err)


class TBRunner(object):
    """Testbench runner."""
    def __init__(self, num_workers, runner_sleep=15):
        """Initialize runner.

        Args:
            num_workers: The number of worker processes to be spawned
            runner_sleep: How many seconds the runner will sleep before
                next run iteration
        """
        self.num_workers = num_workers
        self.runner_sleep = runner_sleep

    @provide_session
    def _run_pool(self, session=None):
        """Create a multiprocessing pool and run testbenches."""
        with multiprocessing.Pool(processes=self.num_workers, maxtasksperchild=5) as pool:
            while True:
                waiting = session.query(Testbench).filter(Testbench.state=='WAIT')
                logger.info("_run_pool: Found %s waiting Testbenches" % str(waiting.count()))
                running = session.query(Testbench).filter(Testbench.state=='RUN')
                logger.info("_run_pool: Found %s running Testbenches" % str(running.count()))

                for tb in waiting:
                    # Change state to RUN
                    tb.state = 'RUN'
                    session.commit()
                    # Run the tb in pool
                    pool.apply_async(
                            _run_testbench,
                            (tb.id,),
                            callback=_tb_success_callback,
                            error_callback=_tb_error_callback)

                logger.info("TBRunner.run: sleep %s seconds now" % self.runner_sleep)
                time.sleep(self.runner_sleep)

    def run(self):
        """"""
        try:
            logger.info("Running TBRunner with %s workers" % self.num_workers)
            self._run_pool()
        except KeyboardInterrupt as ke:
            # TODO: Should here be some tear down?
            logger.error("Got keyboard interrupt, terminating TBRunner")
            raise
        except Exception as e:
            logger.error("Catched exception in TBRunner.run: %s" % e)
            raise
        finally:
            logger.info("Exiting TBRunner.")
