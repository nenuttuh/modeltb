import importlib
import logging
import os
import sys

from argparse import ArgumentParser
from datetime import datetime as dt
from modeltb.utils import db as db_utils
from modeltb import exceptions, models, settings, tbrunner
from sqlalchemy.orm.exc import NoResultFound


logger = logging.getLogger(__name__)
DEFAULT_NUM_WORKERS = 3


def run(args):
    """Start the modeltb runner."""
    runner = tbrunner.TBRunner(num_workers=args.num_workers)
    runner.run()


@db_utils.provide_session
def run_tb(args, session=None):
    """Run a single testbench."""
    tb_id = args.tb_id
    slurm = args.slurm

    # Get the testbench instance
    try:
        tb = session.query(models.Testbench).filter(models.Testbench.id==tb_id).one()
    except NoResultFound:
        logger.warning("run_tb: no model with id %s was found" % tb_id)

    # Run the tb
    try:
        tb.run(slurm=slurm)
    except exceptions.ModeltbException as me:
        logger.warning(me)

    logger.info("cli.run_tb done")


def _import_ext_pkg(args):
    """If args.model_pkg is defined, import it from current working dir.

    This is needed, when using modeltb as an external dependency.
    """
    if args.model_pkg:
        logger.info("modeltb: importing package %s" % args.model_pkg)
        sys.path.append(os.getcwd())
        try:
            settings.MODEL_PKG = importlib.import_module(args.model_pkg)
        except:
            logger.error("_import_ext_pkg: could not import package %s" % args.model_pkg)
            raise


def makemigrations(args):
    """Create migrations for external module."""
    if not args.model_pkg:
        logger.error("Cannot run makemigrations if no external module is defined.")
        return None
    print("{} creating migrations for {}".format(dt.now(), args.model_pkg))
    db_utils.makemigrations(args.msg)
    print("{} makemigrations done".format(dt.now()))


def createdb(args):
    """Create the database from scratch.
    
    This will skip the migrations, and create all tables at once,
    and setting Alembic 'head' to current.
    """
    print("{} Creating the database...".format(dt.now()))
    db_utils.createdb()
    print("{} Database created".format(dt.now()))


def upgradedb(args):
    """Run the migrations.
    
    The migrations are run if the state of database differs from
    Alembic head.
    """
    print("{} Starting db upgrade...".format(dt.now()))
    db_utils.upgradedb()
    print("{} Db upgrade done".format(dt.now()))


def resetdb(args):
    """Reset the database.
    
    Drop all tables and recreate them.
    """
    print("{} Reset db...".format(dt.now()))
    db_utils.resetdb()
    print("{} Db reset done".format(dt.now()))


def get_parser():
    parser = ArgumentParser()
    subparsers = parser.add_subparsers()
    
    parser_run = subparsers.add_parser('run')
    parser_run.add_argument('--num_workers', type=int, default=DEFAULT_NUM_WORKERS)
    parser_run.add_argument('model_pkg', type=str, nargs='?')
    parser_run.set_defaults(func=run)

    parser_run_tb = subparsers.add_parser('run_tb')
    parser_run_tb.add_argument('--tb_id', type=str)
    parser_run_tb.add_argument('--slurm', action='store_true', help='Run tb in slurm')
    parser_run_tb.add_argument('model_pkg', type=str, nargs='?')
    parser_run_tb.set_defaults(func=run_tb)

    parser_makemigrations = subparsers.add_parser('makemigrations')
    parser_makemigrations.add_argument('--msg', default='auto')
    parser_makemigrations.add_argument('model_pkg', type=str, nargs='?')
    parser_makemigrations.set_defaults(func=makemigrations)

    parser_createdb = subparsers.add_parser('createdb')
    parser_createdb.add_argument('model_pkg', type=str, nargs='?')
    parser_createdb.set_defaults(func=createdb)

    parser_upgradedb = subparsers.add_parser('upgradedb')
    parser_upgradedb.add_argument('model_pkg', type=str, nargs='?')
    parser_upgradedb.set_defaults(func=upgradedb)

    parser_resetdb = subparsers.add_parser('resetdb')
    parser_resetdb.add_argument('model_pkg', type=str, nargs='?')
    parser_resetdb.set_defaults(func=resetdb)

    return parser


def run_cli():
    parser = get_parser()
    args = parser.parse_args()
    _import_ext_pkg(args)
    args.func(args)


if __name__ == '__main__':
    run_cli()
