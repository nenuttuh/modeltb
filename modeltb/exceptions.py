"""Custom exceptions for ModelTB"""


class ModeltbException(Exception):
    pass


class ModeltbConfigException(ModeltbException):
    pass
