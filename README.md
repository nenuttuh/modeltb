# ModelTB

A testbench framework for developing machine learning models in Python.

## Installation

At the moment, modeltb is installed with pip from the source repository:

`pip install git+ssh://git@bitbucket.org/nenuttuh/modeltb.git#egg=modeltb`

For an example project using modeltb, see https://bitbucket.org/nenuttuh/modeltb-example 

### Running tests

Clone the repository. In repository root, run:

`python setup.py test`

## Configuration

ModelTB is configured through `MODELTB_HOME/modeltb.cfg`. Default configuration is created from `config_templates/default-modeltb.cfg`. The config file is automatically generated if it does not exist. You can change defaults by changing the values in the file.

## Usage

ModelTB provides a commandline interface with `modeltb` command. An automatic testbench discovery and run tool is provided by `modeltb run`. It starts a python program which polls the database for new Testbench instances, and runs them. One can also do this manually by calling `Testbench.run()`.

For a testbench to be able to run, instances of `Model` and `Dataset` need to be given when instantiating a `Testbench`.

For modeltb to be able to find the package where custom classes reside, the commands need to be provided with the package. I.e. run the commands from a location which contains the package directory, and provide the package name importing the models for the command. E.g. to start runner from a directory containing package `example`, run:

`modeltb run example_pkg`

### `MODELTB_HOME`

When first run, modeltb installs its configuration into path in `MODELTB_HOME` environment variable which, if not defined, defaults to `~/modeltb`. If you want to install to somewhere else, just set the env var before running any commands.

### Initialization of database

Before `modeltb` can be used, a database with schema including the models needs to be created. At the moment, SQLite is used as the database, but as `modeltb` uses SQLAlchemy ORM, it should be easy to change it to e.g. Postgres. To create the database for `example_pkg` run:

`modeltb createdb example_pkg`

### Running with testbench runner

Start the runner as follows:

`modeltb run --num-workers=3 example_pkg`

This will start a process that polls the database for new testbenches, and runs them in external processes using Python's multiprocessing module.

### Running manually

Run with command line interface:

`modeltb run_tb --tb_id <testbench id> example_pkg`

Or in Python:

```python
tb_id = <testbench id>
testbench = session.query(Testbench).filter(Testbench.id == tb_id).one()
testbench.run()
```

### Slurm support

Modeltb provides utilities to run testbenches in [Slurm](https://slurm.schedmd.com/overview.html). To configure a testbench to run in Slurm, set `Testbench.slurm` to `True`. Or, if using the CLI, add `--slurm` switch to the command:

`modeltb run_tb --tb_id <testbench id> --slurm example_pkg`

## Architecture

![ModelTB class diagram](./doc/model-tb-class.svg)

**Fig 1.** Class diagram of the model-tb architecture.

![Run testbench sequence diagram](./doc/run-tb-sequence.svg)

**Fig 2.** Sequence diagram for running a testbench.

