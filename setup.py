from setuptools import setup, find_packages, Command

import os
import subprocess


class CleanCommand(Command):
    """Customized extra force clean command."""
    user_options = []
    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('rm -vrf ./build ./dist ./*.egg-info')
        # Need to use subprocess.call with shell=True because of the pipe
        subprocess.call('find . -name __pycache__ -type d | xargs rm -vrf', shell=True)


def do_setup():
    setup(
        name='modeltb',
        version='0.1.0.dev1',
        description='A testbench framework for developing machine learning models',
        packages=find_packages(exclude=['tests*']),
        include_package_data=True,
        install_requires=[
            'alembic>=0.9',
            'sqlalchemy>=1.1',
        ],
        test_suite='tests',
        cmdclass={
            'xclean': CleanCommand
        },
        entry_points={
            'console_scripts': [
                'modeltb=modeltb.cli:run_cli',
            ],
        },
    )


if __name__ == "__main__":
    do_setup()
