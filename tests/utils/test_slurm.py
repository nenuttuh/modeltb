import unittest
import subprocess
import modeltb.configuration as config
from modeltb.models import Testbench
from modeltb.utils.slurm import commands
from modeltb import settings
from unittest import mock
from collections import namedtuple


class TestSlurm(unittest.TestCase):

    def setUp(self):
        config.load_test_config()
        self.tb = Testbench()
        Output = namedtuple('Output', 'stdout returncode')
        self.sp_out = Output(stdout=b'The output', returncode=0)

    def test_create_cpu_script(self):
        script_str, script_file = commands.create_script_str(self.tb)
        self.assertIsInstance(script_str, str)
        self.assertIsInstance(script_file, str)

    def test_create_gpu_script(self):
        self.tb.gpu = True
        script_str, script_file = commands.create_script_str(self.tb)
        self.assertIsInstance(script_str, str)
        self.assertIsInstance(script_file, str)

    def test_run_tb_in_slurm_success(self):
        # Mock the actual call to rerun the tb in slurm
        subprocess.run = mock.MagicMock(return_value=self.sp_out)
        self.tb.slurm = True
        self.tb.state = 'SUCCESS'
        session = settings.Session()
        session.add(self.tb)
        session.commit()

        success = commands.run_tb_in_slurm(self.tb)
        self.assertTrue(success)

    def test_run_tb_in_slurm_failure(self):
        subprocess.run = mock.MagicMock(return_value=self.sp_out)
        self.tb.slurm = True
        self.tb.state = 'FAILED'
        self.tb.gpu = True
        session = settings.Session()
        session.add(self.tb)
        session.commit()

        success = commands.run_tb_in_slurm(self.tb)
        self.assertFalse(success)
