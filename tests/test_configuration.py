import unittest
import modeltb.configuration as config


class TestConfig(unittest.TestCase):

    @unittest.skip(
        "This will not work if load_test_config is done somewhere before this test is executed.")
    def test_default_config(self):
        from modeltb import configuration as conf
        self.assertEqual(
            conf.get('database', 'sqlalchemy_url'),
            'sqlite:///{}/modeltb.db'.format(conf.MODELTB_HOME)
        )

    def test_get_config(self):
        config.load_test_config()
        self.assertEqual(
            config.conf.get('database', 'sqlalchemy_url'),
            'sqlite:///{}/unittests.db'.format(config.MODELTB_HOME)
        )

    def test_slurm_config(self):
        gpu = config.conf['slurm_gpu']
        cpu = config.conf['slurm_cpu']

        cpu_keys = set([
            'n_tasks',
            'cpus_per_task',
            'max_duration',
            'mem_kb',
            'partition'
        ])
        gpu_keys = cpu_keys | {'n_gpus'}
        
        self.assertEqual(set(cpu), cpu_keys)
        self.assertEqual(set(gpu), gpu_keys)
