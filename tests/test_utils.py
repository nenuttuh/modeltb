import unittest
import sqlalchemy

from modeltb import utils


class TestDbUtils(unittest.TestCase):

    def test_provide_session(self):
        
        # Create a function that should be provided with a Session
        @utils.db.provide_session
        def f_with_session(session=None):
            self.assertTrue(isinstance(session, sqlalchemy.orm.session.Session))
        
        # The session should come from utils.db.provide_session
        f_with_session()
