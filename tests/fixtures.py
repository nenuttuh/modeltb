"""Simple models for testing the modeltb framework."""
from sqlalchemy import Column, Integer, ForeignKey
from modeltb import models
import math


class DummyTb(models.Testbench):
    id = Column(Integer, ForeignKey('testbench.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'dummytb'}

    @classmethod
    def get_score(self, y_pred, test_data):
        y_true = test_data[1]
        return sum([p == t for (p, t) in zip(y_pred, y_true)]) / len(y_pred)

    def save_results(self, y_pred, test_data):
        pass


class DummyModel(models.Model):
    id = Column(Integer, ForeignKey('model.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'dummymodel'}

    def fit(self, train_data):
        pass

    def predict(self, xs):
        # Predict always ones
        return [1 for _ in xs]


class DummyDataset(models.Dataset):
    id = Column(Integer, ForeignKey('dataset.id'), primary_key=True)
    __mapper_args__ = {'polymorphic_identity': 'dummydataset'}

    def train_test_split(self, test_size=0.1, seed=None):
        print("Test size = %s, type is %s" % (str(test_size), str(type(test_size))))
        xs = [1 for _ in range(100)]
        ys = [1 for _ in range(100)]
        thr = math.floor(test_size * len(ys))
        # return (xs[:thr], xs[thr:], ys[:thr], ys[thr:])
        return (xs[:thr], ys[:thr]), (xs[thr:], ys[thr:])
