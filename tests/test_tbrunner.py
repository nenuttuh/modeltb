import unittest
import time
from modeltb import configuration as conf
from modeltb import models, settings, tbrunner
from modeltb.exceptions import ModeltbException
from unittest import mock
from tests.fixtures import DummyTb, DummyDataset, DummyModel


# A hacky exception to be raised from while True loop
class SuccessException(Exception):
    pass


class TestTBRunner(unittest.TestCase):

    def setUp(self):
        settings.load_test_settings()
        models.Base.metadata.create_all(settings.engine)
        self.session = settings.Session()

        self.dataset = DummyDataset()
        self.model = DummyModel()
        self.runner = tbrunner.TBRunner(num_workers=1)

    def tearDown(self):
        models.Base.metadata.drop_all(settings.engine)

    @unittest.skip("Multiprocessing testing issues. Fix later.")
    def test_finds_and_calls_waiting(self):
        tb = DummyTb()
        tb.model = self.model
        tb.dataset = self.dataset
        self.session.add_all([self.model, tb, self.dataset])
        self.session.commit()

        # Raise this 'SuccessException' thing so that TBRunner.run() will exit its loop...
        models.Testbench.run = mock.MagicMock(return_value=None, side_effect=SuccessException("Testbench.run called"))
        with self.assertRaises(SuccessException):
            self.runner.run()

    @unittest.skip("Multiprocessing testing issues. Fix later.")
    def test_without_model_and_dataset(self):
        tb = DummyTb()
        # Leave model and dataset undefined, will lead to exception
        self.session.add(tb)
        self.session.commit()
        # time.sleep = mock.MagicMock(side_effect=SuccessException("time.sleep called"))
        # tbrunner.logging.warning = mock.MagicMock(side_effect=SuccessException("logging.warning called"))
        with self.assertRaises(SuccessException):
            self.runner.run()
