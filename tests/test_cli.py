import unittest

from modeltb import cli


class TestCli(unittest.TestCase):
    
    def test_parse_run(self):
        parser = cli.get_parser()
        args = parser.parse_args(['run'])
        self.assertEqual(args.num_workers, cli.DEFAULT_NUM_WORKERS)

        args = parser.parse_args('run --num_workers=5'.split())
        self.assertEqual(args.num_workers, 5)

        self.assertTrue(args.func is cli.run)

    def test_parse_run_tb(self):
        parser = cli.get_parser()
        args = parser.parse_args('run_tb --tb_id abc123'.split())
        self.assertEqual(args.tb_id, 'abc123')

        self.assertTrue(args.func is cli.run_tb)

        # Test --slurm switch and model_pkg arg
        args = parser.parse_args('run_tb --tb_id abc123 --slurm foo.bar'.split())
        self.assertTrue(args.slurm)
        self.assertEqual(args.model_pkg, 'foo.bar')

    def test_parse_upgradedb(self):
        parser = cli.get_parser()
        args = parser.parse_args(['upgradedb'])
        self.assertTrue(args.func is cli.upgradedb)
