import unittest
from modeltb import settings, models
from modeltb.exceptions import ModeltbException
from tests.fixtures import DummyTb, DummyDataset, DummyModel


class TestTestbench(unittest.TestCase):
    
    def setUp(self):
        settings.load_test_settings()
        models.Base.metadata.create_all(settings.engine)
        self.session = settings.Session()

        self.tb = DummyTb()
        self.model = DummyModel()
        self.dataset = DummyDataset()

    def tearDown(self):
        models.Base.metadata.drop_all(settings.engine)

    def test_cannot_run_without_dataset(self):
        self.tb.model = self.model
        self.session.add_all([self.tb, self.model, self.dataset])
        self.session.commit()
        with self.assertRaises(ModeltbException):
            self.tb.run()
        
        # Seems that self.tb gets detached from the session when calling tb.run,
        # so need to add it again...
        self.session.add(self.tb)
        self.session.commit()
        self.assertTrue(self.tb.state == 'FAILED')

    def test_cannot_run_without_model(self):
        self.tb.dataset = self.dataset
        self.session.add_all([self.tb, self.model, self.dataset])
        self.session.commit()
        # Check that the tb is in WAIT state before doing stuff..
        self.assertTrue(self.tb.state == 'WAIT')
        with self.assertRaises(ModeltbException):
            self.tb.run()
        
        self.session.add(self.tb)
        self.session.commit()
        self.assertTrue(self.tb.state == 'FAILED')

    def test_success_with_simple_model(self):
        self.tb.dataset = self.dataset
        self.tb.model = self.model
        self.session.add_all([self.tb, self.model, self.dataset])
        self.session.commit()

        # Check the initial state
        self.assertTrue(self.tb.state == 'WAIT')

        # Run the tb
        self.tb.run()

        # Check the results
        self.session.add_all([self.tb, self.model, self.dataset])
        self.assertTrue(self.tb.state == 'SUCCESS')
        self.assertEqual(self.tb.score, 1.0)
